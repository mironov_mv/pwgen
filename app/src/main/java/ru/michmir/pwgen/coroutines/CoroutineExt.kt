package ru.michmir.pwgen.coroutines

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataScope
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.experimental.ExperimentalTypeInference

suspend fun <T> onComputation(block: suspend CoroutineScope.() -> T) = withContext(Dispatchers.Default, block)

@UseExperimental(ExperimentalTypeInference::class)
fun <T> ViewModel.liveDataComputation(

    @BuilderInference block: suspend LiveDataScope<T>.() -> Unit
): LiveData<T> = androidx.lifecycle.liveData(context = viewModelScope.coroutineContext + Dispatchers.IO, block = block)