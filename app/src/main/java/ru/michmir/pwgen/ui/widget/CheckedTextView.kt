package ru.michmir.pwgen.ui.widget

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.widget.CheckedTextView
import androidx.core.content.res.use
import ru.michmir.pwgen.R

class CheckedTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : CheckedTextView(context, attrs, defStyleAttr) {

    private var checkedTextAppearanceId: Int = -1
    private var uncheckedTextAppearanceId: Int = -1

    init {
        context.theme.obtainStyledAttributes(attrs, R.styleable.CheckedTextView, 0, 0).use { a ->
            checkedTextAppearanceId = a.getResourceId(R.styleable.CheckedTextView_checkedTextAppearance, -1)
            uncheckedTextAppearanceId = a.getResourceId(R.styleable.CheckedTextView_uncheckedTextAppearance, -1)
        }
        updateTextAppereance(isChecked)
    }

    override fun setChecked(checked: Boolean) {
        super.setChecked(checked)
        updateTextAppereance(checked)
    }

    private fun updateTextAppereance(checked: Boolean) {
        if (checkedTextAppearanceId != -1 && uncheckedTextAppearanceId != -1) {
            val resId = if (checked) checkedTextAppearanceId else uncheckedTextAppearanceId
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                setTextAppearance(resId)
            } else {
                @Suppress("DEPRECATION")
                setTextAppearance(context, resId)
            }
        }
    }

}