package ru.michmir.pwgen.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_password.*
import ru.michmir.pwgen.R

class PasswordViewHolder private constructor(
    override val containerView: View,
    private val clickHandler: (String) -> Unit
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    companion object {

        fun inflate(parent: ViewGroup, clickHandler: (String) -> Unit): PasswordViewHolder = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_password, parent, false)
            .let { view -> PasswordViewHolder(view, clickHandler) }

    }

    fun bind(password: String) {
        passwordTextView.run {
            text = password
            setOnClickListener { clickHandler(password) }
        }
    }

}