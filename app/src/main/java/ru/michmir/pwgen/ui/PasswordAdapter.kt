package ru.michmir.pwgen.ui

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class PasswordAdapter(
    private val clickHandler: (String) -> Unit
) : RecyclerView.Adapter<PasswordViewHolder>() {

    private val items = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = PasswordViewHolder.inflate(parent, clickHandler)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PasswordViewHolder, position: Int) = items[position]
        .let(holder::bind)

    fun swap(items: List<String>) {
        this.items.clear()
        this.items += items
        notifyDataSetChanged()
    }

}