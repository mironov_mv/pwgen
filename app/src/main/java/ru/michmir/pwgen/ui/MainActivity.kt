package ru.michmir.pwgen.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ru.michmir.pwgen.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}
