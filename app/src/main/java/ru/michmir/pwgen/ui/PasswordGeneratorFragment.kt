package ru.michmir.pwgen.ui

import android.content.ClipData
import android.content.ClipDescription
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_password_generator.*
import ru.michmir.pwgen.R
import ru.michmir.pwgen.presentation.PasswordGeneratorViewModel

class PasswordGeneratorFragment : Fragment(R.layout.fragment_password_generator) {

    private val viewModel: PasswordGeneratorViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val passwordAdapter = PasswordAdapter(::onPasswordItemClick)
        passwordsRecyclerView.run {
            layoutManager = GridLayoutManager(requireContext(), 3) // TODO determinate spanCount
            adapter = passwordAdapter
        }

        generateButton.setOnClickListener { viewModel.resetPasswords() }

        viewModel.passwords.observe(this, passwordAdapter::swap)
        viewModel.lowLetters.observe(this, lowLettersTextView::setChecked)
        viewModel.capitalLetters.observe(this, capitalLettersTextView::setChecked)
        viewModel.digitsLetters.observe(this, digitsTextView::setChecked)
        viewModel.specialSymbols.observe(this, specialSymbolsTextView::setChecked)

        lowLettersTextView.setOnClickListener { viewModel.toggleLowLetters() }
        capitalLettersTextView.setOnClickListener { viewModel.toggleCapitalLetters() }
        digitsTextView.setOnClickListener { viewModel.toggleDigits() }
        specialSymbolsTextView.setOnClickListener { viewModel.toggleSpecialSymbols() }

    }

    private fun onPasswordItemClick(password: String) {
        val clip = ClipData.newPlainText(password, password)
        val clipboard = requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.setPrimaryClip(clip)
        Snackbar.make(rootView, R.string.fragment_password_generator_clipboard_message, Snackbar.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        passwordsRecyclerView.adapter = null
        super.onDestroyView()
    }
}
