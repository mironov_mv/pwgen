package ru.michmir.pwgen.presentation

import androidx.lifecycle.*
import ru.michmir.pwgen.coroutines.liveDataComputation
import ru.michmir.pwgen.domain.PasswordGenerator
import ru.michmir.pwgen.domain.models.PwGenConfig

class PasswordGeneratorViewModel : ViewModel() {

    private val _pwGenConfig = MutableLiveData<PwGenConfig>().apply {
        value = PwGenConfig()
    }
    val lowLetters = _pwGenConfig.map(PwGenConfig::useLower)
    val capitalLetters = _pwGenConfig.map(PwGenConfig::useUpper)
    val digitsLetters = _pwGenConfig.map(PwGenConfig::useDigits)
    val specialSymbols = _pwGenConfig.map(PwGenConfig::useSpecialSymbols)

    private val _passwords = _pwGenConfig.switchMap(::resetPasswords)
    val passwords: LiveData<List<String>> = _passwords

    private fun resetPasswords(pwGenConfig: PwGenConfig) = liveDataComputation {
        val result = (0 until pwGenConfig.length)
            .map { PasswordGenerator.generatePassword(pwGenConfig) }
        emit(result)
    }

    fun resetPasswords() {
        _pwGenConfig.value = _pwGenConfig.value
    }

    fun toggleLowLetters() = toggle { pwGenConfig -> pwGenConfig.copy(useLower = !pwGenConfig.useLower) }

    fun toggleCapitalLetters() = toggle { pwGenConfig -> pwGenConfig.copy(useUpper = !pwGenConfig.useUpper) }

    fun toggleDigits() = toggle { pwGenConfig -> pwGenConfig.copy(useDigits = !pwGenConfig.useDigits) }

    fun toggleSpecialSymbols() = toggle { pwGenConfig -> pwGenConfig.copy(useSpecialSymbols = !pwGenConfig.useSpecialSymbols) }

    private fun toggle(transformer: (PwGenConfig) -> PwGenConfig) {
        val oldValue = _pwGenConfig.value ?: return
        val newValue = transformer(oldValue)
        if (selectedOptionCount(newValue) < 1) return
        _pwGenConfig.value = newValue
    }

    private fun selectedOptionCount(pwGenConfig: PwGenConfig): Int {
        var options = 0
        if (pwGenConfig.useLower) options++
        if (pwGenConfig.useUpper) options++
        if (pwGenConfig.useDigits) options++
        if (pwGenConfig.useSpecialSymbols) options++
        return options
    }

}