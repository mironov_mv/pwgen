package ru.michmir.pwgen.domain

import ru.michmir.pwgen.coroutines.onComputation
import ru.michmir.pwgen.domain.models.PwGenConfig
import java.security.SecureRandom

object PasswordGenerator {

    suspend fun generatePassword(pwGenConfig: PwGenConfig = PwGenConfig()): String = onComputation {
        val secureRandom = SecureRandom()
        val asciiCodes = asciiCodes(pwGenConfig)
        val stringBuilder = StringBuilder()
        (0 until  pwGenConfig.length)
            .map { secureRandom.nextInt(asciiCodes.size) }
            .map { index -> asciiCodes[index] }
            .forEach { code -> stringBuilder.appendCodePoint(code) }
        stringBuilder.toString()
    }

    private fun asciiCodes(pwGenConfig: PwGenConfig): List<Int> {
        val asciiCodes = mutableListOf<Int>()
        if (pwGenConfig.useSpecialSymbols) {
            asciiCodes += 33..43
            asciiCodes += 45..47
            asciiCodes += 60..64
            asciiCodes += 91..93
            asciiCodes += 95
        }
        if (pwGenConfig.useDigits) asciiCodes += 48..57
        if (pwGenConfig.useUpper) asciiCodes += 65..90
        if (pwGenConfig.useLower) asciiCodes += 97..122
        return asciiCodes
    }

}