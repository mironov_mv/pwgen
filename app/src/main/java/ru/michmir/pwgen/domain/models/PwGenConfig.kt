package ru.michmir.pwgen.domain.models

data class PwGenConfig(
    val useSpecialSymbols: Boolean = false,
    val useDigits: Boolean = true,
    val useUpper: Boolean = true,
    val useLower: Boolean = true,
    val length: Int = 8
)